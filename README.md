# Hi! Welcome to my world ❤️ 
# 嗨！欢迎来到我的世界 ❤️
I'm Heidi 
也是大家的小蔚姐姐

## About me 关于我
I’m a teacher，and I‘m also a designer.
我是一名老师，同时我也是一名设计师
I have many hobbies, such as：
我有很多爱好，例如：
1. Drawing 绘画
2. Writing 写作
3. Play the piano 弹钢琴
4. Mountain climbing 爬山
5. Boxing 拳击

现在我们换一种形式，用“•"来表达不同的兴趣分类
* Drawing 绘画
* Writing 写作
* Play the piano 弹钢琴
* Mountain climbing 爬山
* Boxing 拳击

### 我最喜欢的动画片是 海绵宝宝
大家可以点击下方⬇️⬇️⬇️链接观看


![ ](/images/海绵宝宝和他的朋友们.jpg)